package bucketmanager

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"strconv"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/utils"
	clientv3 "go.etcd.io/etcd/client/v3"
)

type EtcdBucketManager struct {
	scopeKey    string
	bucketTime  float64
	BucketStore map[string]string
}

/**
* EtcdBucketManager  / NewEtcdBucketManager
* Initialize EtcdBucketManager struct
*/
func NewEtcdBucketManager(scopeKey string, bucketTime float64) EtcdBucketManager {
	etcdBM := EtcdBucketManager{scopeKey, bucketTime, make(map[string]string)}
	return etcdBM
}

/**
* EtcdBucketManager  / CalculateRequestCountFromBucket
* returns total request count for window size
* BucketStore - local BucketStore map
*/
func (e *EtcdBucketManager) CalculateRequestCountFromBucket() int64 {
	var totalRequestMade int64 = 0
	if len(e.BucketStore) > 0 {
		for _, value := range e.BucketStore {
			val, _ := strconv.ParseInt(value, utils.BASE_10, utils.BIT_SIZE_64)
			totalRequestMade = totalRequestMade + val
		}
	}
	return totalRequestMade
}

/**
 * EtcdBucketManager  / GetCurrentBucket
 * epochTime - current timestamp in ms
 * bucketTime - bucketTime
 * @returns current bucket time in milliseconds
 */
func (e *EtcdBucketManager) GetCurrentBucket(epochTime int64) int64 {
	return int64(math.Floor(float64(epochTime)/e.bucketTime) * e.bucketTime)
}

/**
 * EtcdBucketManager  / IncrementBucketCount
 * Increment bucket count for bucketId, store in local BucketStore
 * Push updated bucket count to basePath
 * currentBucketId - currentBucketId
 * basePath - basePath
 * conn - Etcd client
 */
func (e *EtcdBucketManager) IncrementBucketCount(currentBucketId int64, basePath string, conn *clientv3.Client) error {
	//Convert currentBucketId to string
	bucketId := fmt.Sprint(currentBucketId)
	//Increment count of currentBucket
	bucketValue, _ :=  strconv.ParseInt(e.BucketStore[bucketId]  , utils.BASE_10, utils.BIT_SIZE_64)
	//Store in local map
	e.BucketStore[bucketId] = fmt.Sprint(bucketValue + 1)
	bucketStoreStr, err := json.Marshal(e.BucketStore)
	if err != nil {
		return err
	}
	_, err = conn.Put(context.Background(), basePath, strconv.Quote(string(bucketStoreStr)))
	if err != nil {
		return err
	}
	return nil
}