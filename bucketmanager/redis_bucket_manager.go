package bucketmanager

import (
	"fmt"
	"math"
	"strconv"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/utils"
	"github.com/go-redis/redis"
)

type RedisBucketManager struct {
	scopeKey    string
	bucketTime  float64
}

func NewRedisBucketManager(scopeKey string, bucketTime float64) RedisBucketManager {
	redisBM := RedisBucketManager{scopeKey, bucketTime}
	return redisBM
}

/**
 * RedisBucketManager  / GetCurrentBucket
 * epochTime - current timestamp in ms
 * bucketTime - bucketTime
 * @returns bucket start time in milliseconds
 */
func (b *RedisBucketManager) GetCurrentBucket(epochTime int64) int64 {
	return int64(math.Floor(float64(epochTime)/b.bucketTime) * b.bucketTime)
}

/**
* RedisBucketManager  / CalculateRequestCountFromBucket
* scopeKey  - redis hash key
* conn - redis client
* @returns total request count for window size
 */
func (b *RedisBucketManager) CalculateRequestCountFromBucket(conn *redis.Client) (int64, error) {
	result, err := conn.HVals(b.scopeKey).Result()
	if err != nil {
		return 0, err
	}
	var totalRequestMade int64
	for _, value := range result {
		val, _ := strconv.ParseInt(value, utils.BASE_10, utils.BIT_SIZE_64)
		totalRequestMade = totalRequestMade + val
	}
	return totalRequestMade, nil
}

/**
* RedisBucketManager / incrementBucketByUnitAtomically
* scopeKey  - redis hash key
* currentBucketId - redis hash field
* client - redis connection
 */
func (b *RedisBucketManager) IncrementBucketByUnitAtomically(currentBucketId int64, conn *redis.Client) error {
	bucketId := fmt.Sprint(currentBucketId)
	// Transactionally increments key using GET and SET commands.
	for {
		err := conn.Watch(b.executeTransaction(b.scopeKey, bucketId), bucketId)
		if err == nil {
			return err
		} else {
			fmt.Println(err, "retry")
		}
	}
}

func (b *RedisBucketManager) executeTransaction(scopeKey string, bucketId string) func(tx *redis.Tx) error {
	txf := func(tx *redis.Tx) error {
		count := tx.HMGet(scopeKey, bucketId).Val()
		var fieldCount int64 = 0
		if count[0] != nil {
			//convert string to int
			fieldCount, _ = strconv.ParseInt(fmt.Sprint(count[0]), utils.BASE_10, utils.BIT_SIZE_64)
		}
		var m = make(map[string]interface{})
		m[bucketId] = fieldCount + 1

		// runs only if the watched keys remain unchanged
		_, err := tx.Pipelined(func(pipe redis.Pipeliner) error {
			// pipe handles the error case
			pipe.HMSet(scopeKey, m)
			return nil
		})
		return err
	}
	return txf
}
