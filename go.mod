module bitbucket.org/suraj9762/circles-go-rate-limiter

go 1.16

require (
	bitbucket.org/libertywireless/circles-go-rate-limiter v1.0.0
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/streadway/amqp v1.0.0
	go.etcd.io/etcd/client/v3 v3.5.0
)
