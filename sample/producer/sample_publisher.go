package main

import (
	"fmt"
	"net/http"

	"github.com/streadway/amqp"
)

func connect() *amqp.Channel {

	conn, err := amqp.Dial("amqp://raas_stage:VxKponHiy9u1@morn.circles.asia:5672/raas_stage")
	if err != nil {
		fmt.Println("rmq connection failed", err)
	}

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println("Channel conn failed", err)
	}
	return ch
}

func main() {
	//connect to RabbitMQ
	ch := connect()
	count := 0
	//initialize consumer
	//defer rmqConsumer(&lim)

	http.HandleFunc("/publish", func(w http.ResponseWriter, r *http.Request) {
		count++
		body := "msg no:" + fmt.Sprint(count)
		ch.Publish(
			"rate_limit_sw", // exchange
			"rate.limit",    // routing key
			false,           // mandatory
			false,           // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        []byte(body),
			})
		fmt.Println("Published Message :", body)
	})

	err := http.ListenAndServe(":8080", nil)

	if err != nil {
		fmt.Println(err)
	}
}
