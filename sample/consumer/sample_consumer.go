package main

import (
	"errors"
	"fmt"
	"log"
	"time"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/ratelimiter"
	"github.com/streadway/amqp"
)

func main() {
	// RabbitMQ
	rc := RabbitConfig{
		Schema:         "amqp",
		Username:       "raas_stage",
		Password:       "VxKponHiy9u1",
		Host:           "morn.circles.asia",
		Port:           "5672",
		VHost:          "raas_stage",
		ConnectionName: "my_app_name",
	}
	rbt := NewRabbit(rc)
	if err := rbt.Connect(); err != nil {
		log.Fatalln("unable to connect to rabbit", err)
	}
	//rate limiter initialize
	// Consumer
	cc := ConsumerConfig{
		ExchangeName:  "rate_limit_sw",
		ExchangeType:  "direct",
		RoutingKey:    "rate.limit",
		QueueName:     "rate_limit",
		ConsumerName:  "my_app_name",
		ConsumerCount: 1,
		PrefetchCount: 1,
	}
	cc.Reconnect.MaxAttempt = 60
	cc.Reconnect.Interval = 1 * time.Second
	csm := NewConsumer(cc, rbt)
	//new
	if err := csm.Start(); err != nil {
		log.Fatalln("unable to start consumer", err)
	}

	select {}
}

// RABBIT -----------------------------------------------------------------------------------------------
type RabbitConfig struct {
	Schema         string
	Username       string
	Password       string
	Host           string
	Port           string
	VHost          string
	ConnectionName string
}

type Rabbit struct {
	config     RabbitConfig
	connection *amqp.Connection
}

// NewRabbit returns a RabbitMQ instance.
func NewRabbit(config RabbitConfig) *Rabbit {
	return &Rabbit{
		config: config,
	}
}

// Connect connects to RabbitMQ server.
func (r *Rabbit) Connect() error {
	if r.connection == nil || r.connection.IsClosed() {
		con, err := amqp.DialConfig(fmt.Sprintf(
			"%s://%s:%s@%s:%s/%s",
			r.config.Schema,
			r.config.Username,
			r.config.Password,
			r.config.Host,
			r.config.Port,
			r.config.VHost,
		), amqp.Config{Properties: amqp.Table{"connection_name": r.config.ConnectionName}})
		if err != nil {
			return err
		}
		r.connection = con
	}

	return nil
}

// Connection returns exiting `*amqp.Connection` instance.
func (r *Rabbit) Connection() (*amqp.Connection, error) {
	if r.connection == nil || r.connection.IsClosed() {
		return nil, errors.New("connection is not open")
	}
	return r.connection, nil
}

// Channel returns a new `*amqp.Channel` instance.
func (r *Rabbit) Channel() (*amqp.Channel, error) {
	chn, err := r.connection.Channel()
	if err != nil {
		return nil, err
	}

	return chn, nil
}

// CONSUMER ---------------------------------------------------------------------------------------------

type ConsumerConfig struct {
	ExchangeName  string
	ExchangeType  string
	RoutingKey    string
	QueueName     string
	ConsumerName  string
	ConsumerCount int
	PrefetchCount int
	Reconnect     struct {
		MaxAttempt int
		Interval   time.Duration
	}
}

type Consumer struct {
	config ConsumerConfig
	Rabbit *Rabbit
}

// NewConsumer returns a consumer instance.
func NewConsumer(config ConsumerConfig, rabbit *Rabbit) *Consumer {
	return &Consumer{
		config: config,
		Rabbit: rabbit,
	}
}

// Start declares all the necessary components of the consumer and
// runs the consumers. This is called one at the application start up
// or when consumer needs to reconnects to the server.
func (c *Consumer) Start() error {
	con, err := c.Rabbit.Connection()
	if err != nil {
		return err
	}
	//go c.closedConnectionListener(con.NotifyClose(make(chan *amqp.Error)),lim)

	chn, err := con.Channel()
	if err != nil {
		return err
	}

	if err := chn.ExchangeDeclare(
		c.config.ExchangeName,
		c.config.ExchangeType,
		true,  //durable
		false, //autoDelete
		false, //internal
		false, //noWait
		nil,   //args
	); err != nil {
		return err
	}

	if _, err := chn.QueueDeclare(
		c.config.QueueName,
		true,  //durable
		false, //autoDelete
		false, //exclusives
		false, //noWait
		nil,   // args
	); err != nil {
		return err
	}

	if err := chn.QueueBind(
		c.config.QueueName,
		c.config.RoutingKey,
		c.config.ExchangeName,
		false,
		nil,
	); err != nil {
		return err
	}

	if err := chn.Qos(c.config.PrefetchCount, 0, false); err != nil {
		return err
	}

	var dataSourceConfig map[string]string = make(map[string]string)
	dataSourceConfig["type"] = "redis"
	dataSourceConfig["isSentinel"] = "false"
	dataSourceConfig["host"] = "localhost:6379"
	dataSourceConfig["master"] =  "circles_redis"
	//dataSourceConfig["sentinel"] = "sjp-redis-01.circles.life:1126739,sjp-redis-01.circles.life:1126379,sjp-redis-01.circles.life:1126379"
	dataSourceConfig["password"] = "SFMANPiPxihCnhyVxtbGHxxdxe"

	//ETCD Connection
	// dataSourceConfig["type"] = "etcd"
	// dataSourceConfig["basePath"] = "/configuration-manager/reward-service/sg-staging/v1"
	// dataSourceConfig["host"] = "localhost:2379"
	// dataSourceConfig["username"] = ""
	// dataSourceConfig["password"] = ""

	var limit int64 = 5
	var windowTime int64 = 60000
	var buckets int64 = 6
	var scopeKey string = "nexmo"
	rateLimiter, rateLimiterErr := ratelimiter.GetInstance(scopeKey, windowTime, buckets, limit, dataSourceConfig)
	
	if rateLimiterErr != nil {
		fmt.Println("Consumer::", rateLimiterErr)
	}
	
	for i := 1; i <= c.config.ConsumerCount; i++ {
		id := 1
		go c.consume(chn, id, rateLimiter)
	}

	return nil
}

// consume creates a new consumer and starts consuming the messages.
// If this is called more than once, there will be multiple consumers
// running. All consumers operate in a round robin fashion to distribute
// message load.
func (c *Consumer) consume(channel *amqp.Channel, id int, rateLimiter ratelimiter.RateLimiter) {
	msgs, err := channel.Consume(
		c.config.QueueName, //Queue Name
		fmt.Sprintf("%s (%d/%d)", c.config.ConsumerName, id, c.config.ConsumerCount), //consumer
		false, //autoAck
		false, //exclusive
		false, //noLocal
		false, //noWait
		nil,   //args
	)
	if err != nil {
		log.Println(fmt.Sprintf("CRITICAL: Unable to start consumer (%d/%d)", id, c.config.ConsumerCount))

		return
	}
	log.Println("[", id, "] Running ...")
	//forever := make(chan bool)

	for msg := range msgs {
		ok, err := rateLimiter.Allow();
		if err != nil {
			fmt.Println("Consumer allow:", err)
			ok = true
		}
		if ok {
			log.Println("[", id, "] Consumed:", string(msg.Body))
			msg.Ack(true)
		} else {
			msg.Nack(false, true)
			fmt.Println("Error, Request Limit reached")
			time.Sleep(60 * time.Second)
		}
	}

	//<-forever
	log.Println("[", id, "] Exiting ...")
}
