package utils

import (
	"context"
	"errors"
	"time"

	clientv3 "go.etcd.io/etcd/client/v3"
)

var ContextTTL int64

/* Helpers / CalculateContextTTL
*  ContextTTL is windowTime in MS.
*  ContextTTL is set to scopeKeyMetadata as TTL
*  If ContextTTL is less than 1 then return 1 as redis expired does not support
*  negative numbers for expire query
*/
func CalculateContextTTL(windowTime int64) {
	ContextTTL = windowTime / 1000
	if ContextTTL < 1 {
		ContextTTL = 1
	}
}

/* Helpers / MakeTimestamp
*  Returns current timestamp in millisec
*/
func MakeTimestamp() int64 {
	return time.Now().UnixNano() / int64(time.Millisecond)
}

/* Helpers / EtcdLock
*  Set temp lock and assign TTL
*  key - Etcd lock key
*  value - temp value for lock key
*  cli - Etcd client
*/
func EtcdLock(key, value string, cli *clientv3.Client) error {
	lockKeyResponse, lockErr := cli.Get(context.Background(), key)
	if lockErr != nil {
		return lockErr
	}
	 // If the lock is present, return to the lock failed
	 if len(lockKeyResponse.Kvs) > 0 {
		return errors.New("lock is already acquired for key")
	}

	// Set key expiration time
	resp, err := cli.Grant(context.Background(), ETCD_LOCK_TTL)
	if err != nil {
		return err
	}
	// Set key and bind expiration time
	_, err = cli.Put(context.Background(), key, value, clientv3.WithLease(resp.ID))
	if err != nil {
		return err
	}
	return nil
}

/* Helpers / EtcdUnlock
*  key - Etcd lock key
*  cli - Etcd client
*/
func EtcdUnlock(key string, cli *clientv3.Client) error{
	_, err := cli.Delete(context.Background(), key)
	if err != nil {
		return err
	}
	return nil
}