package utils

import "time"

const (
	METADATA_SUFFIX               string = "_metadata"
	DS_KEY_SENTINEL               string = "sentinel"
	DS_KEY_HOST                   string = "host"
	DS_KEY_MASTER                 string = "master"
	DS_KEY_PASSWORD               string = "password"
	DS_KEY_ISSENTINEL             string = "isSentinel"
	DS_KEY_LIMIT                  string = "limit"
	DS_KEY_WINDOW_TIME            string = "windowTime"
	DS_KEY_BUCKETS                string = "buckets"
	DS_TYPE                       string = "type"
	DS_REDIS                      string = "redis"
	DS_ETCD                       string = "etcd"
	TRUE                          string = "true"
	FALSE                         string = "false"
	DS_KEY_BASE_PATH              string = "basePath"
	DS_KEY_SCOPEKEY_METADATA_PATH string = "scopeKeyMetadataPath"
	DS_KEY_LOCK_BASE_PATH         string = "lockKeyBasePath"
	DS_KEY_RATE_LIMITER           string = "/ratelimiter/"
	DS_KEY_METADATA               string = "/metadata"
	DS_KEY_UPDATE_LOCK            string = "/updatelock"
	DS_KEY_BUCKET_COUNT           string = "/bucketcount"
	DS_KEY_USERNAME               string = "username"
	ETCD_LOCK_VALUE				  string = "value"
	ETCD_LOCK_TTL                 int64  = 5
	BASE_10                       int    = 10
	BIT_SIZE_64                   int    = 64
	DialTimeout                          = 100 * time.Second
)