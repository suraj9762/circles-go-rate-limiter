# circles-go-rate-limiter
A sliding window counter based rate limiter in Golang

### About The Library -
It can work for client as well server-side rate limiting.  
It can work with local node specific and global distributed rate limiting.  
It can scale to any number of requests any number of nodes in a distributed system.  
It can be tuned up from “Most Accurate with moderate performance” - to - “Less accurate with better performance” by changing the number of buckets. The higher the buckets number more accurate it will be.  

### Library Usage -
To import the rate limiter library in project –
```
go get bitbucket.org/libertywireless/circles-go-rate-limiter
```
```
import (
 "bitbucket.org/libertywireless/circles-go-rate-limiter/ratelimiter"
)
```

### To initialize sliding window rate limiter instance require few configurations -

* **scopeKey**: For the given scope key rate limiter instance will be created and return. Scope key of type string. For example "nexmo".  
* **Limit**: Limit is the number of requests which will be processed within given window time. Limit is of type int64.  
* **windowTime**: Window time is the interval of time where request will be processed as per defined limit. windowTime should be passed in millisecods.  
* **buckets**: Number of buckets will be created within window time. buckets is of type int64.  
* **dataSourceConfig**: dataSourceConfig stores key, value data such as type, isSentinel, master, sentinel, password. dataSourceConfig is of type map[string]string and expects values in  key, value format mentioned above.  
To use sliding window rate limiter library, it requires datastore connection where the request counts are maintained. This library currently supports redis as datastore and can be extended to other datastore as well.  

### To connect with redis sentinel -
```
dataSourceConfig["type"] = "redis"  // datastore type - redis or etcd
dataSourceConfig["isSentinel"] = "true" // isSentinel accepts true or false. If isSentinel passed as "true" then it will connect to redis sentinel. If "false" then it will make redis connection to single node. To make connection with single redis server then pass
dataSourceConfig["host"] = "ipaddr:6379" instaed of dataSourceConfig["sentinel"].
dataSourceConfig["master"] =  "circles_redis"
dataSourceConfig["sentinel"] = "sjp-redis-01.circles.life:26739,sjp-redis-01.circles.life:26379,sjp-redis-01.circles.life:26379"
dataSourceConfig["password"] = ""
```
### To connect with standalone redis server -
```
dataSourceConfig["isSentinel"] = "false" //true or false should be in string and not bool
dataSourceConfig["host"] = "ipaddr:6379" 
dataSourceConfig["password"] = ""
```
Call GetInstance function to initialize rate limiter instance. GetInstance accepts scopeKey, windowTime, buckets, limit, dataSourceConfig as input and all of them are mandatory. GetInstance returns instance and error object. 
```
rateLimiter, rateLimiterErr := ratelimiter.GetInstance(scopeKey, windowTime, buckets, limit, dataSourceConfig)
```
Call Allow method to check whether request is allowed or not. Allow() returns bool true or false. 

True => Request is allowed.  
False => Request limit is reached, rejected.  
```
isRequestAllowed := rateLimiter.Allow() // Check if rate limit is breached
```
### To run sample code -

Sample files will be found under sample/ folder.

go run sample_publisher.go // Accessible via localhost:8080/publish
Publish messages to RMQ rate_limit queue

go run sample_consumer.go //In sample_consumer lib is imported and used.
```
for msg := range msgs {
		ok, err := rateLimiter.Allow();
		if err != nil {
			fmt.Println("Consumer allow:", err)
		}
		if ok {
			log.Println("[", id, "] Consumed:", string(msg.Body))
			msg.Ack(true)
		} else {
			msg.Nack(false, true)
			fmt.Println("Error, Request Limit reached")
			time.Sleep(30 * time.Second)
		}
	}
```
### Resources -
* [Rate Limiter - Approach](https://libertywireless.atlassian.net/wiki/spaces/BRP/pages/2370241622/Rate+Limiter+-+Approach)
