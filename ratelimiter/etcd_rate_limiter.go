package ratelimiter

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"
	"strings"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/bucketmanager"
	"bitbucket.org/suraj9762/circles-go-rate-limiter/utils"
	clientv3 "go.etcd.io/etcd/client/v3"
)

type EtcdRateLimiter struct {
	config         *rateLimiterConfig
	conn           *clientv3.Client
	bucketmanager  bucketmanager.EtcdBucketManager
	bucketCount    []byte
}

/**
* EtcdRateLimiter / NewEtcdRateLimiter
* Initialize rate limiter config values
*/
func NewEtcdRateLimiter(scopeKey string, windowTime int64, buckets int64, limit int64, dataSourceConfig map[string]string) *EtcdRateLimiter {
	etcdConfig := &rateLimiterConfig{
		scopeKey:         scopeKey,
		windowTime:       windowTime,
		buckets:          buckets,
		limit:            limit,
		dataSourceConfig: dataSourceConfig,
		scopeKeyMetadata: scopeKey + utils.METADATA_SUFFIX,
		bucketTime:       float64(windowTime) / float64(buckets),
	}
	etcdRL := EtcdRateLimiter{config: etcdConfig}
	return &etcdRL
}

/**
* EtcdRateLimiter / connect
* Validate input params and connect to etcd  and returns etcd client or erros if any
*/
func (e *EtcdRateLimiter) connect() (*clientv3.Client, error) {

	if value, ok := e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH]; !ok || (ok && value == "") {
		return nil, errors.New("EtcdRateLimiter:: basePath field is empty, please provide valid basePath")
	}

	if value, ok := e.config.dataSourceConfig[utils.DS_KEY_HOST]; !ok || (ok && value == "") {
		return nil, errors.New("EtcdRateLimiter:: host field is empty, please provide valid hosts")
	}

	//Split etcd string "host:port" into []string type
	etcdAddrs := strings.Split( e.config.dataSourceConfig[utils.DS_KEY_HOST], ",")

	conn, err := clientv3.New(clientv3.Config{
		Endpoints: etcdAddrs,
		DialTimeout: utils.DialTimeout,
		Username: e.config.dataSourceConfig[utils.DS_KEY_USERNAME],
		Password: e.config.dataSourceConfig[utils.DS_KEY_PASSWORD],
	})

	if err != nil {
		fmt.Println("EtcdRateLimiter:: connect:", err)
		return nil, err
	}
	return conn, nil
}

/**
* EtcdRateLimiter / Initialize
* Initialize etcd connection, etcd bucket manager, 
* sync local bucketStore with etcd basePath
* scopeKey - scopeKey
* windowTime - windowTime
* buckets - bucket count
*/
func (e *EtcdRateLimiter) initialize() error {
	//Initialize ETCD connection
	connection, etcdConnErr := e.connect()
	if etcdConnErr != nil {
		return etcdConnErr
	}
	e.conn = connection

	if e.config.limit <= 0 || e.config.windowTime <= 0 || e.config.buckets <= 0 {
		fmt.Println("EtcdRateLimiter:: initialize: limit, windowTime. buckets should be greater than zero")
		return errors.New("EtcdRateLimiter:: initialize: limit, windowTime. buckets should be greater than zero")
	}
	if e.config.scopeKey == "" {
		fmt.Println("EtcdRateLimiter:: initialize: scopeKey should not be empty")
		return errors.New("EtcdRateLimiter:: initialize: scopeKey should not be empty")
	}

	//Create etcd basepath where bucketId and count will be stored
	e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH] = e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH] + utils.DS_KEY_RATE_LIMITER + e.config.scopeKey + utils.DS_KEY_METADATA
	e.config.dataSourceConfig[utils.DS_KEY_LOCK_BASE_PATH] = e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH] + utils.DS_KEY_RATE_LIMITER + e.config.scopeKey + utils.DS_KEY_UPDATE_LOCK
	e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH] = e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH] + utils.DS_KEY_RATE_LIMITER + e.config.scopeKey + utils.DS_KEY_BUCKET_COUNT

	// Initialize bucketStore which will manage bucket and their count
	e.bucketmanager = bucketmanager.NewEtcdBucketManager(e.config.scopeKey, e.config.bucketTime)

	//Sync Etcd Bucket Data with local bucketStore
	bucketKeyValue, err := e.conn.Get(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH])
	if err != nil {
		return err
	}
	if bucketKeyValue.Count > 0 {
		e.bucketCount =bucketKeyValue.Kvs[0].Value
	}

	// Calculate context TTL which will be set to the scopeKeyMetadata
	utils.CalculateContextTTL(e.config.windowTime)
	
	// WatcherRoutine will concurrently start thread which will keep listening for basePath key change
	go e.WatcherRoutine()
	
	validateScopeKeyErr := e.validateScopeKeyMetadata()
	if validateScopeKeyErr != nil {
		return validateScopeKeyErr
	}
	return nil
}

/**
* EtcdRateLimiter / validateScopeKeyMetadata
* limit - max number of requests allowed
* windowTime - time in milliseconds
* buckets - number of buckets
* scopeKeyMetadataPath - Etcd path where /scopekey/metadata saved
* example - scopeKey = "nexmo", scopeKeyMetadata = /nexmo/metadata
*/
func (e *EtcdRateLimiter) validateScopeKeyMetadata() error {
	scopeKeyMetadataValues, err := e.conn.Get(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH])
	if err != nil {
		return err
	}
	//Create local scopeKeyMetadataStore map to store etcd scopeKeyMetadata String into key, value
	//which will be used for comparison
	var scopeKeyMetadataStore map[string]string = make(map[string]string)
	if len(scopeKeyMetadataValues.Kvs) > 0 { 
		s, _ := strconv.Unquote(string(scopeKeyMetadataValues.Kvs[0].Value))
		err := json.Unmarshal([]byte(s), &scopeKeyMetadataStore)
		if err != nil {
			return err
		}
	} else {
		scopeKeyMetadataStore[utils.DS_KEY_LIMIT] = strconv.FormatInt(e.config.limit,utils.BASE_10)
		scopeKeyMetadataStore[utils.DS_KEY_WINDOW_TIME] =  strconv.FormatInt(e.config.windowTime, utils.BASE_10)
		scopeKeyMetadataStore[utils.DS_KEY_BUCKETS] =  strconv.FormatInt(e.config.buckets, utils.BASE_10)
	}

	if len(scopeKeyMetadataValues.Kvs) > 0 {
		//convert string to int for comparison
		lim, _ := strconv.ParseInt(scopeKeyMetadataStore[utils.DS_KEY_LIMIT], utils.BASE_10, utils.BIT_SIZE_64)
		wTime, _ := strconv.ParseInt(scopeKeyMetadataStore[utils.DS_KEY_WINDOW_TIME], utils.BASE_10, utils.BIT_SIZE_64)
		buckts, _ := strconv.ParseInt(scopeKeyMetadataStore[utils.DS_KEY_BUCKETS], utils.BASE_10, utils.BIT_SIZE_64)
		if lim != e.config.limit || wTime != e.config.windowTime || buckts != e.config.buckets {
			validateScopeKeyErrMsg := e.config.scopeKeyMetadata + `configs, limit, windowTime, buckets not configurred properly`
			return errors.New(validateScopeKeyErrMsg)
		}
	} else {
		lease := clientv3.NewLease(e.conn)
		grantResp, _ := lease.Grant(context.Background(), utils.ContextTTL)
		scopeKeyMetadataStoreStr, err := json.Marshal(scopeKeyMetadataStore)
		if err != nil {
			return err
		}
		_, err = e.conn.Put(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH], strconv.Quote(string(scopeKeyMetadataStoreStr)), clientv3.WithLease(grantResp.ID))
		if err != nil {
			fmt.Printf("EtcdRateLimiter:: validateScopeKeyMetadata: %s", err)
			return err
		}
	}
	return nil
}

/**
* EtcdRateLimiter / refreshContext
* Refresh scopeKeyMetadata context for every request.
*/
func (e *EtcdRateLimiter) refreshContext() error {
	scopeKeyMetadataValues, err := e.conn.Get(context.Background(),e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH])
	if err != nil {
		return err
	}
	lease := clientv3.NewLease(e.conn)
	grantResp, _ := lease.Grant(context.Background(), utils.ContextTTL)
	var scopeKeyMetadataStore map[string]string = make(map[string]string)
	if len(scopeKeyMetadataValues.Kvs) <= 0 {
		scopeKeyMetadataStore[utils.DS_KEY_LIMIT] = strconv.FormatInt(e.config.limit,utils.BASE_10)
		scopeKeyMetadataStore[utils.DS_KEY_WINDOW_TIME] =  strconv.FormatInt(e.config.windowTime, utils.BASE_10)
		scopeKeyMetadataStore[utils.DS_KEY_BUCKETS] =  strconv.FormatInt(e.config.buckets, utils.BASE_10)
		scopeKeyMetadataStoreStr, err := json.Marshal(scopeKeyMetadataStore)
		if err != nil {
			return err
		}
		_, err = e.conn.Put(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH], strconv.Quote(string(scopeKeyMetadataStoreStr)), clientv3.WithLease(grantResp.ID))
		if err != nil {
			fmt.Printf("EtcdRateLimiter:: refreshContext: %s", err)
			return err
		}
	} else {
		_, err = e.conn.Put(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_SCOPEKEY_METADATA_PATH], string(scopeKeyMetadataValues.Kvs[0].Value), clientv3.WithLease(grantResp.ID))
		if err != nil {
			fmt.Printf("EtcdRateLimiter:: refreshContext: %s", err)
			return err
		}
	}
	return nil
}

/**
* EtcdRateLimiter / allow
* allow checks whether request is allowed or not
* Returns true or false
*/
func (e *EtcdRateLimiter) Allow() (bool, error) {

	err := utils.EtcdLock(e.config.dataSourceConfig[utils.DS_KEY_LOCK_BASE_PATH], utils.ETCD_LOCK_VALUE, e.conn)
	if err != nil {
		fmt.Println("EtcdRateLimiter:: Allow:",err)
		return false, err
	}

	if len(e.bucketCount) > 0 {
		s, _ := strconv.Unquote(string(e.bucketCount))
		err := json.Unmarshal([]byte(s), &e.bucketmanager.BucketStore)
		if err != nil {
			fmt.Println("EtcdRateLimiter:: Allow:",err)
		}
	}

	allBuckets := len(e.bucketmanager.BucketStore)
	//get current timestamp in milliseconds
	currentTimestamp := utils.MakeTimestamp()
	oldestPossibleEntry := currentTimestamp - e.config.windowTime

	if allBuckets > 0 {
		for key := range e.bucketmanager.BucketStore {
			bucketId, _ := strconv.ParseInt(key, utils.BASE_10, utils.BIT_SIZE_64)
			if bucketId < oldestPossibleEntry {
				//Delete old buckets from local bucketStore
				delete(e.bucketmanager.BucketStore, key)
				//Sync local bucketStore with etcd datastore basepath
				result, err := json.Marshal(e.bucketmanager.BucketStore)
				if err != nil {
					return false, err
				}
				_, err = e.conn.Put(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH],strconv.Quote(string(result)))
				if err != nil {
					return false, err
				}
			}
		}
	}
	totalRequestMade := e.bucketmanager.CalculateRequestCountFromBucket()
	if totalRequestMade >= e.config.limit {
		err = utils.EtcdUnlock(e.config.dataSourceConfig[utils.DS_KEY_LOCK_BASE_PATH], e.conn)
		if err != nil {
			return false, err
		}
		return false, nil
	}

	currentBucket := e.bucketmanager.GetCurrentBucket(currentTimestamp)
	err = e.bucketmanager.IncrementBucketCount(currentBucket, e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH], e.conn)
	if err != nil {
		return false, err
	}
	err = e.refreshContext()
	if err != nil {
		return false, err
	}
	err = utils.EtcdUnlock(e.config.dataSourceConfig[utils.DS_KEY_LOCK_BASE_PATH], e.conn)
	if err != nil {
		return false, err
	}
	return true, nil
}

/**
* EtcdRateLimiter / WatcherRoutine
* WatcherRoutine will keep listening to the basePath key,
* Update local bucket store if change in value
*/
func (e *EtcdRateLimiter) WatcherRoutine() {
	ch := e.conn.Watch(context.Background(), e.config.dataSourceConfig[utils.DS_KEY_BASE_PATH], clientv3.WithPrefix())
	for {
		select {
		case result := <-ch:
			for _, ev := range result.Events {
				if ev.IsCreate() || ev.IsModify() {
					e.config.mtx.Lock()
					e.bucketCount = ev.Kv.Value
					e.config.mtx.Unlock()
				}
			}
		}
	}
}