package ratelimiter

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/bucketmanager"
	"bitbucket.org/suraj9762/circles-go-rate-limiter/utils"
	"github.com/go-redis/redis"
)

/* RedisRateLimiter
*  config - Redis Rate Limiter configurations
*  conn - Redis Client
*  bucketmanager - Redis bucket manager
 */
type RedisRateLimiter struct {
	config        *rateLimiterConfig
	conn          *redis.Client
	bucketmanager bucketmanager.RedisBucketManager
}

/**
* RedisRateLimiter / NewRedisRateLimiter
* Initialize redis rate limiter config params and return RedisRateLimiter instance
*/
func NewRedisRateLimiter(scopeKey string, windowTime int64, buckets int64, limit int64, dataSourceConfig map[string]string) *RedisRateLimiter {
	redisConfig := &rateLimiterConfig{
		scopeKey: scopeKey,
		windowTime: windowTime,
		buckets: buckets,
		limit: limit,
		dataSourceConfig: dataSourceConfig,
		scopeKeyMetadata: scopeKey + utils.METADATA_SUFFIX,
		bucketTime:   float64(windowTime) / float64(buckets),
	}
	redisRL := RedisRateLimiter{config: redisConfig}
	return &redisRL
}

/**
* RedisRateLimiter / connect
* Validate input params and connect to redis
*/
func (r *RedisRateLimiter) connect() (*redis.Client, error) {

	if r.config.dataSourceConfig[utils.DS_KEY_ISSENTINEL] != utils.TRUE && r.config.dataSourceConfig[utils.DS_KEY_ISSENTINEL] != utils.FALSE {
		return nil, errors.New("RedisRateLimiter:: isSentinel field is mandatory to initialize redis connection, accepts string true or false")
	}

	var conn *redis.Client
	if r.config.dataSourceConfig[utils.DS_KEY_ISSENTINEL] == utils.TRUE {
		//Validate input params for redis sentinel
		if r.config.dataSourceConfig[utils.DS_KEY_MASTER] == "" || r.config.dataSourceConfig[utils.DS_KEY_SENTINEL] == "" {
			return nil, errors.New("RedisRateLimiter:: RedisDataStore config values empty or invalid")
		}

		sentinelAddr := strings.Split(r.config.dataSourceConfig[utils.DS_KEY_SENTINEL], ",")
		conn = redis.NewFailoverClient(&redis.FailoverOptions{
			MasterName:    r.config.dataSourceConfig[utils.DS_KEY_MASTER] ,
			SentinelAddrs: sentinelAddr,
			Password:      r.config.dataSourceConfig[utils.DS_KEY_PASSWORD],
		})
	}
	
	if r.config.dataSourceConfig[utils.DS_KEY_ISSENTINEL] == utils.FALSE {
		//Validate input params for redis
		if r.config.dataSourceConfig[utils.DS_KEY_HOST] == "" {
			return nil, errors.New("RedisRateLimiter:: RedisDataStore config values empty or invalid")
		}

		conn = redis.NewClient(&redis.Options{
			Addr: r.config.dataSourceConfig[utils.DS_KEY_HOST],
			Password:  r.config.dataSourceConfig[utils.DS_KEY_PASSWORD],
		})
	}
	_, err := conn.Ping().Result()
	if err != nil {
		fmt.Println("RedisRateLimiter:: getRedisConnection:", err)
		return nil, err
	}
	return conn, nil
}

/**
* RedisRateLimiter / Initialize
* Initialize redis connection
* scopeKey - scopeKey
* limit - no of request within window
* windowTime - windowTime
* buckets - bucket count
*/
func (r *RedisRateLimiter) initialize() error {
	//Initialize redis connection
	connection, redisConnErr := r.connect()
	if redisConnErr != nil {
		return redisConnErr
	}
	r.conn = connection

	if r.config.limit <= 0 || r.config.windowTime <= 0 || r.config.buckets <= 0 {
		fmt.Println("RedisRateLimiter:: initialize: limit, windowTime. buckets should be greater than zero")
		return errors.New("RedisRateLimiter:: initialize: limit, windowTime. buckets should be greater than zero")
	}
	if r.config.scopeKey == "" {
		fmt.Println("RedisRateLimiter:: initialize: scopeKey should not be empty")
		return errors.New("RedisRateLimiter:: initialize: scopeKey should not be empty")
	}

	utils.CalculateContextTTL(r.config.windowTime)
	r.bucketmanager = bucketmanager.NewRedisBucketManager(r.config.scopeKey, r.config.bucketTime)

	//Validate scopeKey
	validateScopeKeyErr := r.validateScopeKeyMetadata()
	if validateScopeKeyErr != nil {
		return validateScopeKeyErr
	}
	return nil
}

/**
* RedisRateLimiter / validateScopeKey
* limit @int64 - max number of requests allowed
* windowTime @int64 - time in milliseconds
* buckets @int64 - number of buckets
* scopeKeyMetadata @string - redis hash key
* example - scopeKey = "nexmo", scopeKeyMetadata = nexmo_metadata
*/
func (r *RedisRateLimiter) validateScopeKeyMetadata() error {
	scopeKeyExists, err := r.conn.Exists(r.config.scopeKeyMetadata).Result()
	if err != nil {
		fmt.Printf("RedisRateLimiter:: validateScopeKeyMetadata: %s", err)
		return err
	}
	if scopeKeyExists == 1 {
		values, hgetErr := r.conn.HGetAll(r.config.scopeKeyMetadata).Result()
		if hgetErr != nil {
			fmt.Printf("RedisRateLimiter:: validateScopeKeyMetadata: %s", hgetErr)
			return hgetErr
		}
		//convert string to int for comparison
		lim, _ := strconv.ParseInt(values[utils.DS_KEY_LIMIT], utils.BASE_10, utils.BIT_SIZE_64)
		wTime, _ := strconv.ParseInt(values[utils.DS_KEY_WINDOW_TIME], utils.BASE_10, utils.BIT_SIZE_64)
		buckts, _ := strconv.ParseInt(values[utils.DS_KEY_BUCKETS], utils.BASE_10, utils.BIT_SIZE_64)
		if lim != r.config.limit || wTime != r.config.windowTime || buckts != r.config.buckets {
			validateScopeKeyErrMsg := r.config.scopeKeyMetadata + `configs, limit, windowTime, buckets not configurred properly`
			return errors.New(validateScopeKeyErrMsg)
		}
	} else {
		metadataHashObj := make(map[string]interface{})
		metadataHashObj[utils.DS_KEY_LIMIT] = r.config.limit
		metadataHashObj[utils.DS_KEY_WINDOW_TIME] = r.config.windowTime
		metadataHashObj[utils.DS_KEY_BUCKETS] = r.config.buckets
		err = r.conn.HMSet(r.config.scopeKeyMetadata, metadataHashObj).Err()
		if err != nil {
			fmt.Printf("RedisRateLimiter:: validateScopeKeyMetadata: %s", err)
			return err
		}
		err = r.conn.Expire(r.config.scopeKeyMetadata, time.Duration(utils.ContextTTL)*time.Second).Err()
		if err != nil {
			fmt.Printf("RedisRateLimiter:: validateScopeKeyMetadata: %s", err)
			return err
		}
	}
	return nil
}

/**
* RedisRateLimiter / refreshContext
* Refresh scopeKeyMetadata context every request
* limit @int64 - max number of requests allowed
* windowTime @int64 - time in milliseconds
* buckets @int64 - number of buckets
* metadataHash  - redis hash key
*/
func (r *RedisRateLimiter) refreshContext() error {
	scopeKeyExists, err := r.conn.Exists(r.config.scopeKeyMetadata).Result()
	if err != nil {
		fmt.Println("RedisRateLimiter:: refreshContext:", err)
		return err
	}
	if scopeKeyExists == 0 {
		metadataHashObj := make(map[string]interface{})
		metadataHashObj[utils.DS_KEY_LIMIT] = r.config.limit
		metadataHashObj[utils.DS_KEY_WINDOW_TIME] = r.config.windowTime
		metadataHashObj[utils.DS_KEY_BUCKETS] = r.config.buckets
		err = r.conn.HMSet(r.config.scopeKeyMetadata, metadataHashObj).Err()
		if err != nil {
			fmt.Printf("RedisRateLimiter:: refreshContext: %s", err)
			return err
		}
	}
	err = r.conn.Expire(r.config.scopeKeyMetadata, time.Duration(utils.ContextTTL)*time.Second).Err()
	if err != nil {
		return err
	}
	return nil
}

/**
* RedisRateLimiter / allow
* allow checks whether request is allowed or not
* Returns true or false
*/
func (r *RedisRateLimiter) Allow() (bool, error) {
	r.config.mtx.Lock()
	defer r.config.mtx.Unlock()
	allBuckets, err := r.conn.HKeys(r.config.scopeKey).Result()
	if err != nil {
		fmt.Printf("RedisRateLimiter:: allow: %s", err)
		return false, err
	}
	//get current timestamp in milli seconds
	currentTimestamp := utils.MakeTimestamp()
	oldestPossibleEntry := currentTimestamp - r.config.windowTime
	if len(allBuckets) > 0 {
		for _, bucket := range allBuckets {
			buck, _ := strconv.ParseInt(bucket, utils.BASE_10, utils.BIT_SIZE_64)
			if buck < oldestPossibleEntry {
				//Delete old buckets
				err = r.conn.HDel(r.config.scopeKey, bucket).Err()
				if err != nil {
					return false, err
				}
			}
		}
	}
	var totalRequestMade int64
	totalRequestMade, err = r.bucketmanager.CalculateRequestCountFromBucket(r.conn)
	if err != nil {
		fmt.Printf("RedisRateLimiter:: allow: %s", err)
		return false, err
	}

	if totalRequestMade >= r.config.limit {
		return false, nil
	}

	currentBucket := r.bucketmanager.GetCurrentBucket(currentTimestamp)
	err = r.bucketmanager.IncrementBucketByUnitAtomically(currentBucket, r.conn)
	if err != nil {
		return false, err
	}

	err = r.refreshContext()
	if err != nil {
		return false, err
	}
	return true, nil
}
