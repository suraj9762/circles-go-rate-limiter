package ratelimiter

import (
	"sync"
)

/* RateLimiter
* Allow - Allow will check whether request is within limit.
* Allow will return bool and error.
* In case of request within limit bool value will be true and error will be nil.
* In case of request outside limit bool value will be false and error will be nil
* In case of any error bool will be false and error will be not nil.
*
* initialize - Internal function to initialize rate limiter.
* It will create respective datasource connection ex. Redis or Etcd.
* It will initialize respective bucket manager ex. RedisBucketManager or EtcdBucketManager.
* It will validate scope key metadata.
* Incase of error it will return error object else nil.
 */
type RateLimiter interface {
	Allow() 	(bool, error)
	initialize() error
}

/* RateLimiter / rateLimiterConfig
*  Initialize config for respective rate limiter
*  Ex :- Redis or Etcd
*/
type rateLimiterConfig struct {
	scopeKey   	 		string
	windowTime   		int64
	buckets      		int64
	limit 				int64
	dataSourceConfig 	map[string]string
	scopeKeyMetadata 	string
	bucketTime   		float64
	mtx           		sync.Mutex
}