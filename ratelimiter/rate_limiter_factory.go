package ratelimiter

import (
	"errors"
	"fmt"

	"bitbucket.org/suraj9762/circles-go-rate-limiter/utils"
)

var instanceMap map[string]RateLimiter = make(map[string]RateLimiter)

/**
* RateLimiterFactory / GetInstance
* Initialize instance as per data store type and return
* scopeKey (Manadatory) - Scope key for the instance will be created
* windowTime (Manadatory) - Define window duration to process x no of reqs within windowTime
* buckets (Manadatory) - Number of buckets will be created for given window as per requests
* limit (Manadatory) - Number of requests limit for window
* dataSourceConfig (Mandatory) - Data source config params for datastore connectivity
**/
func GetInstance(scopeKey string, windowTime int64, buckets int64, limit int64, dataSourceConfig map[string]string) (RateLimiter, error) {
	var instance RateLimiter
	var err error = nil
	if instanceMap[scopeKey] != nil {
		instance = instanceMap[scopeKey]
	} else {
		switch dataSourceConfig[utils.DS_TYPE] {
		case utils.DS_REDIS:
			instance = NewRedisRateLimiter(
				scopeKey,
				windowTime,
				buckets,
				limit,
				dataSourceConfig,
			)
			err = instance.initialize()
			if err != nil {
				instance = nil
			}
			instanceMap[scopeKey] = instance
		case utils.DS_ETCD:
			instance = NewEtcdRateLimiter(
				scopeKey,
				windowTime,
				buckets,
				limit,
				dataSourceConfig,
			)
			err = instance.initialize()
			if err != nil {
				instance = nil
			}
			instanceMap[scopeKey] = instance

		default:
			fmt.Println("RateLimitController:: Incorrect datasource",dataSourceConfig)
			instance = nil
			err = errors.New("RateLimitController:: Incorrect datasource")
		}
	}
	return instance, err
}